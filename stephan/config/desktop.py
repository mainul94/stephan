# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Stephan",
			"color": "green",
			"icon": "fa fa-users",
			"type": "module",
			"label": _("Stephan")
		}
	]
