# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import frappe.www.printview
from .utils.printpreview import _get_print_format
__version__ = '0.0.1'

frappe.www.printview.get_print_format = _get_print_format