import frappe

def execute():
    frappe.db.sql_ddl("alter table tabItem modify kauflink VARCHAR(999)")
    frappe.db.sql_ddl("alter table tabItem modify kauflink_url VARCHAR(999)")
    frappe.db.sql_ddl("alter table tabItem modify google_drive_ref VARCHAR(999)")