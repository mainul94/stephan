import frappe
from .stock_balance import get_items
from erpnext.stock.report.stock_ledger.stock_ledger import get_columns, get_opening_balance, get_stock_ledger_entries, _

def stock_ledger_execute(filters=None):
    columns = update_get_columns()
    items = get_items(filters)
    sl_entries = get_stock_ledger_entries(filters, items)
    item_details = get_item_details(items, sl_entries)
    opening_row = get_opening_balance(filters, columns)

    data = []
    if opening_row:
        data.append(opening_row)

    for sle in sl_entries:
        item_detail = item_details[sle.item_code]

        data.append([sle.date, sle.item_code, item_detail.item_name, item_detail.item_group,
            item_detail.sortiment, item_detail.topseller_kategorie, item_detail.brand, item_detail.description,
             sle.warehouse, item_detail.stock_uom, sle.actual_qty, sle.qty_after_transaction,
            (sle.incoming_rate if sle.actual_qty > 0 else 0.0),
            sle.valuation_rate, sle.stock_value, sle.voucher_type, sle.voucher_no,
            sle.batch_no, sle.serial_no, sle.project, sle.company])

    return columns, data

def update_get_columns():
    columns = get_columns()
    columns.insert(4, _("Sortiment") + ":Link/Sortiment:100")
    columns.insert(5, _("Topseller Kategorie") + ":Link/Topseller Kategorie:100")

    return columns

def get_item_details(items, sl_entries):
    item_details = {}
    if not items:
        items = list(set([d.item_code for d in sl_entries]))

    if not items:
        return item_details

    for item in frappe.db.sql("""
        select name, item_name, description, item_group, brand, stock_uom, sortiment, topseller_kategorie
        from `tabItem`
        where name in ('{0}')
        """.format("', '".join(items).encode('utf-8').strip()), as_dict=1):
            item_details.setdefault(item.name, item)

    return item_details
