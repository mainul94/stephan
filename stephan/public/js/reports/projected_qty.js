/**
 * Created by mainul on 6/12/18.
 */
frappe.query_reports['Stock Projected Qty'].filters.splice(0,1);
frappe.query_reports['Stock Projected Qty'].filters.pop();
let filters = [
    {
        fieldname: "item_name",
        fieldtype: "Data",
        label: __("Item Name")
    },
    {
        fieldname: "default_supplier",
        fieldtype: "Link",
        options: "Supplier",
        label: __("Default Supplier")
    },
    {
        fieldname: "item_group",
        fieldtype: "Link",
        options: "Item Group",
        label: __("Item Group")
    },
    {
        fieldname: "product_group",
        fieldtype: "Link",
        options: "Product Group",
        label: __("Product Group")
    },
    {
        fieldname: "barcode",
        fieldtype: "Data",
        label: __("Barcode")
    },
    {
        fieldname: "sortiment",
        fieldtype: "Link",
        options: "Sortiment",
        label: __("Sortiment")
    },
    {
        fieldname: "asin",
        fieldtype: "Data",
        label: __("Asin")
    },
    {
        fieldname: "topseller_kategorie",
        fieldtype: "Link",
        options: "Topseller Kategorie",
        label: __("Topseller Kategorie")
    },
    {
        fieldname: "project",
        fieldtype: "Link",
        options: "Project",
        label: __("Project")
    }
]
filters.forEach(f => {
    frappe.query_reports['Stock Projected Qty'].filters.splice(999, 0, f)
})