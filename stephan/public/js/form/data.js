frappe.ui.form.ControlData = frappe.ui.form.ControlData.extend({
    set_disp_area: function(value) {
        this._super(value)
        if (typeof this.df.options !==  "undefined" && this.df.options.toLowerCase()==="url") {
            let formated_value = $(this.disp_area).html()
            let html = `<div class="ellipsis">
                <i class="fa fa-paperclip"></i>
                <a class="attached-file-link" target="_blank" href="${formated_value}">${formated_value}</a>
            </div>`;
            this.disp_area && $(this.disp_area).html(html);
            if(this.disp_area && value)
                $(this.disp_area).toggle(true);
        }
    }
})
frappe.ui.form.LinkSelectorForItem = frappe.ui.form.LinkSelector.extend({
    search: function () {
		var args = {
			txt: this.dialog.fields_dict.txt.get_value(),
			searchfield: "name"
		};
		var me = this;

		if (this.target.set_custom_query) {
			this.target.set_custom_query(args);
        }
		// load custom query from grid
		if (this.target.is_grid && this.target.fieldinfo[this.fieldname]
			&& this.target.fieldinfo[this.fieldname].get_query) {
			$.extend(args,
				this.target.fieldinfo[this.fieldname].get_query(cur_frm.doc));
		}
        args.query = "stephan.utils.search.item_query"
		frappe.link_search(this.doctype, args, function (r) {
			var parent = me.dialog.fields_dict.results.$wrapper;
			parent.empty();
			if (r.values.length) {
				$.each(r.values, function (i, v) {
					var row = $(repl('<div class="row link-select-row">\
                        <div class="col-xs-4"> <b><a href="#">%(name)s</a></b></div>\
                        <div class="col-xs-1"> \
                        <span class="avatar avatar-small">\
                        <span class="avatar-frame" style="background-image: url(%(image)s)"></span></span></div>\
						<div class="col-xs-7">\
							<span class="text-muted">%(values)s</span></div>\
						</div>', {
                            name: v[0],
                            image:v.pop(),
							values: v.splice(1).join(", ")
						})).appendTo(parent);
					row.find("a")
						.attr('data-value', v[0])
						.click(function () {
							var value = $(this).attr("data-value");
							var $link = this;
							if (me.target.is_grid) {
								// set in grid
								me.set_in_grid(value);
							} else {
								if (me.target.doctype)
									me.target.parse_validate_and_set_in_model(value);
								else {
									me.target.set_input(value);
									me.target.$input.trigger("change");
								}
								me.dialog.hide();
							}
							return false;
						})
				})
			} else {
				$('<p><br><span class="text-muted">' + __("No Results") + '</span>'
					+ (frappe.model.can_create(me.doctype) ?
						('<br><br><a class="new-doc btn btn-default btn-sm">'
							+ __("Make a new {0}", [__(me.doctype)]) + "</a>") : '')
					+ '</p>').appendTo(parent).find(".new-doc").click(function () {
						frappe.new_doc(me.doctype);
					});
			}
		}, this.dialog.get_primary_btn());

	}
})
frappe.ui.form.Grid = frappe.ui.form.Grid.extend({
	set_multiple_add: function(link, qty) {
		if(this.multiple_set) return;
		var me = this;
		var link_field = frappe.meta.get_docfield(this.df.options, link);
		var btn = $(this.wrapper).find(".grid-add-multiple-rows");

		// show button
		btn.removeClass('hide');

        // open link selector on click
        let args = {
            doctype: link_field.options,
            fieldname: link,
            qty_fieldname: qty,
            target: me,
            txt: ""
        }
		btn.on("click", function() {
            if (args.doctype=="Item") {
                new frappe.ui.form.LinkSelectorForItem(args);
            }else {
                new frappe.ui.form.LinkSelector(args);
            }
			return false;
		});
		this.multiple_set = true;
	}
})