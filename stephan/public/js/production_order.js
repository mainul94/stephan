frappe.provide('stephan')

frappe.ui.form.on("Production Order", {
	setup: function(frm) {		
		// Set query for warehouses
		frm.set_query("default_source_warehouse", function() {
			return {
				filters: {
					'company': frm.doc.company,
				}
			}
        });
    }
});

stephan.ProductionOrder = frappe.ui.form.Controller.extend({
    default_source_warehouse: function() {
        if (!this.frm.doc.default_source_warehouse)
            return ;
        for(let i =0; typeof this.frm.doc.required_items !== "undefined" && i < this.frm.doc.required_items.length && !this.frm.doc.required_items[i].source_warehouse; i++) {
            let row = frappe.get_doc(this.frm.doc.required_items[i].doctype, this.frm.doc.required_items[i].name)
            row.source_warehouse = this.frm.doc.default_source_warehouse
            erpnext.utils.copy_value_in_all_row(this.frm.doc, row.doctype, row.name, 'required_items', 'source_warehouse')
            break
        }
    },
    validate: function() {
        this.default_source_warehouse()
    }
})

$.extend(cur_frm.cscript, new stephan.ProductionOrder({frm: cur_frm}))
