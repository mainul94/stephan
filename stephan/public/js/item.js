frappe.ui.form.on("Item", {
    onload_post_render:function(){
		var section_head = $('.section-head').find("a").filter(function(){ return $(this).text() == "Website" ;}).parent()
		console.log("section head", section_head)
		section_head.on("click", function(){
			console.log($(this).hasClass("collapsed"))
		})
	},
    setup: function(frm) {
        frm.set_df_property('webseite_url', 'read_only', 1)
        frm.add_fetch('druckdateien', 'google_drive_ordner_url', 'druckdatei_google_drive_datei_url', 'druckprodukt')
        frm.add_fetch('druckdateien', 'schnittdateien', 'schnittdateien', 'druckprodukt')
        frm.add_fetch('druckdateien', 'schnittdateien_google_drive_datei_url', 'schnittdatei_google_drive_datei_url', 'druckprodukt')
        frm.add_fetch('schnittdateien', 'google_drive_datei_url', 'schnittdatei_google_drive_datei_url', 'druckprodukt')
        frm.add_fetch('druckdateien', 'ist_kontrolliert', 'ist_kontrolliert', 'druckprodukt')
        frm.add_fetch('druckdateien', 'datum_kontrolle', 'datum_kontrolle', 'druckprodukt')
    },
    refresh: function(frm) {
        frm.set_df_property('barcode', 'read_only', (frm.doc.barcode?1:0));
        console.log($("[data-fieldname='kauflink']")[0]);
        console.log($("[data-fieldname='kauflink']")[1]);
        console.log(document.querySelectorAll('[data-fieldname="kauflink"]')[1].style.height);
        document.querySelectorAll('[data-fieldname="kauflink"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="standardpreis_konfektionierung"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="versandkosten_eigenversand"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="versandkosten_fba_de"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="versandklasse"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="laengste_seite_in_cm"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="mittlere_seite_in_cm"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="kuerzeste_seite_in_cm"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="volumen_in_cm3"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="monatl_amz_lg_q1_bis_q3"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="monatl_amz_lg_q4"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="versandgewicht_in_gramm"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="kosten_logistikaufschlag"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="mindestmenge_einsendung"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="amazon_de_small_and_light_sku"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="amazon_de_fbm_preis"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="amazon_de_fba_preis"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="weitere_werkstatten_kommentar"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="amazon_de_fbm_preis"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="amazon_de_fba_preis"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="kosten_logistikaufschlag"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="ext_sku_lebenshilfe_trier_"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="ext_lagerplatz_lebenshilfe_trier"]')[1].style.height='27px';
        document.querySelectorAll('[data-fieldname="ext_lagerplatz_jg_logistik_neuwied"]')[1].style.height='27px';
    },
    validate: function(frm)  {
        if (frm.doc.publish_in_hub) {
            frm.set_value('publish_in_hub', 0)
        }
        frm.events.set_barcode(frm)
    },
    asin: function (frm) {
        frm.set_value('amazon_de_url', 'https://amazon.de/dp/' + (frm.doc.asin || ""))
        frm.set_value('amazon_usa_url', 'https://amazon.com/dp/' + (frm.doc.asin || ""))
    },
    wocoommerce_id: function(frm) {
        frm.set_value('webseite_url', "https://papierdrachen.de/?post_type=product&p="+(frm.doc.wocoommerce_id || ""))
    },
    ebay_de_id: function(frm) {
        frm.set_value('ebay_url', "https://www.ebay.de/itm/"+(frm.doc.ebay_de_id || ""))
    },
    item_code: function(frm) {
        // frm.set_value('artikel_nummer', frm.doc.item_code)
    },
    // artikel_nummer: function (frm) {
    //     frm.set_value('google_drive_ref', "https://drive.google.com/drive/search?q="+(frm.doc.artikel_nummer || ""))
    // },
    kauflink: function(frm) {
        // frm.set_value('kauflink_url', frm.doc.kauflink)
    },
    ean: function(frm) {
        frm.events.set_barcode(frm)
    },
    fnsku: function(frm) {
        frm.events.set_barcode(frm)
    },set_barcode: function(frm) {
        // let code = ''
        // if (frm.doc.ean) {
        //     code += 'EAN: ' + frm.doc.ean
        // }
        // if (frm.doc.fnsku) {
        //     code += (frm.doc.ean? ' ':'') + 'FNSKU: ' + frm.doc.fnsku
        // }
        // if(!code.trim()) {
        //     return
        // }
        // if (typeof frm.doc.barcodes === 'undefined' || !frm.doc.barcodes.length) {
        //     frm.add_child('barcodes', {
        //         'barcode': code
        //     })
        // }
        // frm.doc.barcodes[0].barcode = code
        // frm.refresh_field('barcodes')
    }
})