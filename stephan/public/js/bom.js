cur_frm.add_fetch('item', 'barcode', 'barcode')
cur_frm.add_fetch('item', 'product_group', 'product_group')
cur_frm.add_fetch('item', 'default_supplier', 'default_supplier')

frappe.ui.form.on("BOM", {
    refresh: function(frm) {
        if (!frm.doc.__islocal) {
            frm.add_custom_button(__("Update Images"), function(){
                frm.events.update_bom_from_item(frm, {
                    'image':'image'
                })
            })
            frm.add_custom_button(__("Update Item Name"), function(){
                frm.events.update_bom_from_item(frm, {
                    'item_name':'item_name'
                })
            })
            frm.add_custom_button(__("Update VR"), function() {
				frm.events.update_vr(frm);
			});
             frm.add_custom_button(__("Download PDF"), function() {
				frm.events.download_pdf(frm);
			});
        }
    },
    update_bom_from_item(frm, fields) {
        frappe.call({
            method: "stephan.utils.bom.update_item",
            args: {
                bom: frm.doc.name,
                fields: fields
            },
            freeze: true,
            callback: r => {
                if(r['message']){
                    if(typeof r.message === "string") r.message = JSON.parse(r.message);
                    frappe.model.sync(r.message)
                    cur_frm.refresh()
                }
            }
        })
    },
    update_vr:function (frm) {
        console.log("update vr!");
        return frappe.call({
			method: "stephan.stephan.bom.update_cost_",
			freeze: true,
			args: {
			    docname:frm.docname,
				update_parent: true,
				from_child_bom:false,
				save: false
			},
			callback: function(r) {
				refresh_field("items");
				if(!r.exc) frm.refresh_fields();
                location.reload();
			}
		});
    },
    download_pdf:function (frm) {
             //  frappe.call({
			// 	method: "stephan.stephan.make_pdf.queue_generate_spreadsheet",
			// 	args:{"docname":cur_frm.doc.name,"doctype":"BOM"},
			// 	freeze: true
			// })
        frappe.call({
				method: "stephan.stephan.make_pdf.make_pdf_boms",
				args:{"docname":cur_frm.doc.name,"doctype":"BOM"},
				freeze: true,
                callback: function (r) {
				    console.log("DOWNLOAD BOMS AS PDF");
						console.log(r);
						if (r.message) {
							$(frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (frm.fields_dict['download_link']) {
                            		$(frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
					}
			})
    },
    download_items:function(frm){
	    console.log("hello items");
        frappe.call({
				method: "stephan.stephan.bom.download_table",
				args:{"docname":cur_frm.doc.name},
				freeze: true,
            callback:function (r) {
				    console.log(r);
                			if (r.message) {
							$(frm.fields_dict['download_link'].wrapper).html("");
                            var _html = '';

                            for(var i=0;i<r.message.files.length;i++)
                            {
                                console.log(r.message.files[i].path);
                                _html +='<a id="downloadleadcsv'+i.toString()+'" ' +
                                    'href="'+r.message.files[i].path+'" download > jpg link </a>' +
										'<script type="text/javascript">' +
                                    'document.getElementById("downloadleadcsv'+i.toString()+'").click(); </script>'

                            }

                            console.log(_html);

							 if (frm.fields_dict['download_link']) {
                            		$(frm.fields_dict['download_link'].wrapper)
                                        .html(_html);

							 }
						}
            }
			})
    }
})