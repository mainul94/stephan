# -*- coding: utf-8 -*-
# Copyright (c) 2018, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import urllib
from frappe.utils import nowdate, cint, cstr
from frappe.utils.nestedset import NestedSet
from frappe.website.render import clear_cache
from erpnext.utilities.product import get_qty_in_stock


class ProductGroup(NestedSet):
	nsm_parent_field = 'parent_product_group'

	def autoname(self):
		self.name = self.product_group_name

	def on_update(self):
		NestedSet.on_update(self)
		self.validate_name_with_item()
		if not (frappe.flags.in_patch or frappe.flags.in_install or frappe.flags.in_import):
			self.validate_one_root()


	def on_trash(self):
		NestedSet.on_trash(self)

	def validate_name_with_item(self):
		if frappe.db.exists("Item", self.name):
			frappe.throw(frappe._("An item exists with same name ({0}), please change the item group name or rename the item").format(self.name), frappe.NameError)

@frappe.whitelist(allow_guest=True)
def get_product_list_for_group(product_group=None, start=0, limit=10, search=None):
	child_groups = ", ".join(['"' + frappe.db.escape(i[0]) + '"' for i in get_child_groups(product_group)])

	# base query
	query = """select I.name, I.item_name, I.item_code, I.route, I.image, I.website_image, I.thumbnail, I.product_group,
			I.description, I.web_long_description as website_description, I.is_stock_item,
			case when (S.actual_qty - S.reserved_qty) > 0 then 1 else 0 end as in_stock, I.website_warehouse,
			I.has_batch_no
		from `tabItem` I
		left join tabBin S on I.item_code = S.item_code and I.website_warehouse = S.warehouse
		where I.show_in_website = 1
			and I.disabled = 0
			and (I.end_of_life is null or I.end_of_life='0000-00-00' or I.end_of_life > %(today)s)
			and (I.variant_of = '' or I.variant_of is null)
			and (I.product_group in ({child_groups})
			or I.name in (select parent from `tabWebsite Product Group` where product_group in ({child_groups})))
			""".format(child_groups=child_groups)
	# search term condition
	if search:
		query += """ and (I.web_long_description like %(search)s
				or I.item_name like %(search)s
				or I.name like %(search)s)"""
		search = "%" + cstr(search) + "%"

	query += """order by I.weightage desc, in_stock desc, I.modified desc limit %s, %s""" % (start, limit)

	data = frappe.db.sql(query, {"product_group": product_group,"search": search, "today": nowdate()}, as_dict=1)

	data = adjust_qty_for_expired_items(data)

	return [get_item_for_list_in_html(r) for r in data]


def adjust_qty_for_expired_items(data):
	adjusted_data = []

	for item in data:
		if item.get('has_batch_no') and item.get('website_warehouse'):
			stock_qty_dict = get_qty_in_stock(
				item.get('name'), 'website_warehouse', item.get('website_warehouse'))
			qty = stock_qty_dict.stock_qty[0][0]
			item['in_stock'] = 1 if qty else 0
		adjusted_data.append(item)

	return adjusted_data



def get_child_groups(product_group_name):
	product_group = frappe.get_doc("Product Group", product_group_name)
	return frappe.db.sql("""select name
		from `tabProduct Group` where lft>=%(lft)s and rgt<=%(rgt)s
			and show_in_website = 1""", {"lft": product_group.lft, "rgt": product_group.rgt})

def get_item_for_list_in_html(context):
	# add missing absolute link in files
	# user may forget it during upload
	if (context.get("website_image") or "").startswith("files/"):
		context["website_image"] = "/" + urllib.quote(context["website_image"])

	context["show_availability_status"] = cint(frappe.db.get_single_value('Products Settings',
		'show_availability_status'))

	products_template = 'templates/includes/products_as_grid.html'
	if cint(frappe.db.get_single_value('Products Settings', 'products_as_list')):
		products_template = 'templates/includes/products_as_list.html'

	return frappe.get_template(products_template).render(context)

def get_group_item_count(product_group):
	child_groups = ", ".join(['"' + i[0] + '"' for i in get_child_groups(product_group)])
	return frappe.db.sql("""select count(*) from `tabItem`
		where docstatus = 0 and show_in_website = 1
		and (product_group in (%s)
			or name in (select parent from `tabWebsite Product Group`
				where product_group in (%s))) """ % (child_groups, child_groups))[0][0]


def get_parent_product_groups(product_group_name):
	product_group = frappe.get_doc("Product Group", product_group_name)
	return 	[{"name": frappe._("Home"), "route":"/"}]+\
		frappe.db.sql("""select name, route from `tabProduct Group`
		where lft <= %s and rgt >= %s
		and show_in_website=1
		order by lft asc""", (product_group.lft, product_group.rgt), as_dict=True)
